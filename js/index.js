
//地球
let marker
let sandbox

let brun = false;
let  audio;


let getEarth =  () => {
    let options = {
        altizureApi:{
            // altizure sdk developer key. Contact Altizure to get your own key.
            key: '7MkQf8UggsPaadvrlKALspJWZejZAJOLHn3cnIy'
        },
        // camera
        camera: {
            // camera start position
            poseTo: {
                lat: 30.947870295755546,
                lng: 120.88925225766249,
                alt: 10000000,
                north: 0,
                tilt: 60
            },
            // camera target position
            flyTo: {
                alt: 100000000,
                lat: 28.245833,
                lng: 102.027778,
                north: 0,
                tilt: 72.39667031677064
            },
            time: 3000
        },
        // render items
        renderItems: {
            earth: true, // earth globe
            featureInView: false, // altizure featured projects
            orbitRing: true, // earth orbit ring
            background: {
                transparent: true,
                opacity: 0,
            }
        }
    }

    sandbox = new altizure.Sandbox('page-content', options)
    sandbox.add('AltizureProjectMarker', {
        pid: '5849104597b73e0b090c01e8',
        pose: {
            position: {lng: 102.025, lat: 28.245833, alt: 0}
        },
        scale: 1
    }).then(m => {
        marker = m
        setTimeout(() => sandbox.camera.lookAt(marker, 30, 60, 1000),3500)
    })

    // use Altizure logo in the tag
    let tag3 = new altizure.TagMarker({
        imgUrl: 'public/img/logo-1024.png',
        position: {lng: 102.025, lat: 28.245833, alt: 35.8978480932066},
        sandbox: sandbox,
        scale: 2
    })
    tag3.interactable = true
    tag3.on('mouseenter', function(e)  { tag3.scale = 3 })
    tag3.on('mouseleave', function(e) { tag3.scale = 2 })






    // add obj models
/*    rocket = new altizure.OBJMarker({
            position: {
                lng: 102.027778,
                lat: 28.245833,
                alt: 0
            },
            sandbox: sandbox,
            name: 'arbor',
            shape: 'CUSTOMIZE',
            objUrl: 'public/object/raw.obj',
            mtlUrl: 'public/object/raw.mtl',
            upDir: {x: 0,y: 1,z: 0},
            "scale": 30
        }
    );
    rocket.dim();*/

	 sandbox.add('AltizureProjectMarker', {pid: '5c4026afa9beaf347178f5c6',position: {
                lng: 102.027778,
                lat: 28.245833,
                alt: 0
            },
			"scale": 30}).then(m => {
        rocket = m;
		rocket.dim();

	 })



    document.addEventListener("keydown", onkeyDown, false);


    function onkeyDown(e){

        if(e.key==="a"){
          ///  var alt =rocket.position.alt + ss;
          //  rocket.position={lng: 102.027778,
          //      lat: 28.245833,
          //      alt: alt};
		  
		  aud = new Audio('public/audio/dao.mp3');

		   aud.play();

		  aud.onended = function() {

				brun = true;
				audio = new Audio('public/audio/sending.mp3');
				audio.loop = true;
				audio.play();
			};

        }

    }

}

//月球
let getMoon = () => {

}


//火箭
let getRocket = () => {

}

let animate = () => {

	requestAnimationFrame( animate );


	if(brun){
	
		var alt =rocket.position.alt + 1;

		if(alt<3500){

			rocket.position={lng: 102.027778,
                lat: 28.245833,
                alt: alt};

			var calt = sandbox.camera.pose.alt+3;

			sandbox.camera.pose = {alt: calt}

		
		}else{
			brun = false;
			audio.pause();
		}
            
	}

}




//初始化
let init = () => {
    getEarth()
}
init()
animate()